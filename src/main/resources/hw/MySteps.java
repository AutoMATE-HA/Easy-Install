package steps;

import org.jbehave.core.annotations.Given;
import org.jbehave.core.annotations.Then;
import org.jbehave.core.annotations.When;
import org.testng.Assert;

import com.incluit.automate.core.exceptions.PropertyException;
import com.incluit.automate.core.utils.ReadProperty;


public class MySteps {

    @Given("print start banner")
    public void printStartBanner() {
        System.out.println("-------- Start TC --------");
    }
    
    @When("print in cosole my name '$name'")
    public void printStartBanner(String name) {
        System.out.println("The name to print is: " + name);        
    }
    
    @Then("exist the variable '$key' in the global properties file")
    public void checkKey(String key) throws PropertyException {
        Assert.assertNotNull(ReadProperty.getProperty(key));
    }
    
}
