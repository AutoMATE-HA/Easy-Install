package com.incluit.automate.easy.install.util;

import java.io.BufferedReader;
import java.io.BufferedWriter;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileWriter;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;

import com.incluit.automate.easy.install.Core;

/**
 * Pom Utils class
 * @author pmallo
 */
public class PomDependeciesUtils {

    /**
     * Add dependency in pom
     * @param dependency to add
     * @throws Exception
     */
    private static void addDependencyInPom(String dependency) throws Exception{

        File pom = new File(Core.PROJECT_PATH + File.separator + Constants.POM_XML);
        
        InputStream inputStream = new FileInputStream(pom);

        BufferedReader rdr = new BufferedReader(new InputStreamReader(
                inputStream));
        String line = null;
        StringBuffer buffer = new StringBuffer();
        try {
            while ((line = rdr.readLine()) != null) {
                if(line.contains("<dependencies>")) {
                    buffer.append(line + "\n" + dependency);
                } else {
                    buffer.append(line + "\n");
                }
                
            }
            rdr.close();
        } catch (IOException e) {
            e.printStackTrace();
        }
        
        BufferedWriter out = new BufferedWriter(new FileWriter(pom));
        out.write(buffer.toString());
        out.flush();
        out.close();
    }
    
    /**
     * Add a dependency in pom
     * @param moduleName artifactId
     * @param moduleVersion version
     * @throws Exception
     */
    public static void addDependency(String moduleName, String moduleVersion) throws Exception {
        addDependencyInPom("           <dependency>\n"
                + "              <groupId>" + Constants.DEFAULT_GROUP_ID + "</groupId>\n"
                + "              <artifactId>" + moduleName + "</artifactId>\n"
                + "              <version>" + moduleVersion + "</version>\n"
                + "           </dependency>\n");
        
    }
}
