package com.incluit.automate.easy.install.util;

import java.io.BufferedReader;
import java.io.File;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.nio.file.Files;
import java.nio.file.Paths;
import java.nio.file.attribute.PosixFilePermissions;

import org.apache.commons.io.IOUtils;

import com.incluit.automate.easy.install.Core;
import com.incluit.automate.easy.install.util.steps.CreateAutomateStructure;

/**
 * File Utils
 * @author pmallo
 */
public class FileUtils {

    /**
     * Copy an file
     * @param file to copy
     * @param folder to copy file
     * @throws Exception
     */
    public static void copyFile(String file, String folder)
            throws Exception {

        File fileToCopy = new File(Core.PROJECT_PATH + folder + File.separator
                + file.replaceAll(Constants.HELLO_WORLD_FOLDER, ""));
        InputStream inputStream = CreateAutomateStructure.class.getClass()
                .getResourceAsStream(file);
        byte[] data = IOUtils.toByteArray(inputStream);
        org.apache.commons.io.FileUtils.writeByteArrayToFile(fileToCopy, data);
    }

    /**
     * Download a jar from Gitlab
     * @param artifactId dependency artifactId
     * @param version dependency version
     * @throws Exception
     */
    public static void downloadJarAndInstall(String artifactId, String version)
            throws Exception {

        File sh = new File(Constants.PROJECT_BASE_PATH + File.separator
                + Constants.DOWNLOAD_JAR_NAME);

        org.apache.commons.io.FileUtils.writeStringToFile(sh,
                Constants.DOWNLOAD_JAR_CONTENT);
        Files.setPosixFilePermissions(Paths.get(sh.getAbsolutePath()),
                PosixFilePermissions
                        .fromString(Constants.DOWNLOAD_JAR_SH_PERMISSION));

        ProcessBuilder pb = new ProcessBuilder("./"
                + Constants.DOWNLOAD_JAR_NAME, artifactId + "-" + version,
                artifactId, version, artifactId + "-" + version);
        pb.directory(new File(System.getProperty("user.dir")));

        WaitingBanner.printBanner();
        Process p;
        p = pb.start();

        BufferedReader reader = new BufferedReader(new InputStreamReader(
                p.getInputStream()));
        String s;
        while ((s = reader.readLine()) != null) {
            System.err.println(s);
        }
        WaitingBanner.stopBanner(true);
        sh.delete();
    }
}
