package com.incluit.automate.easy.install.util;

/**
 * Waiting banner controller 
 * @author pmallo
 */
public class WaitingBanner {
    
    /**
     * <true> the banner is active
     */
    private static boolean active = true;
    
    /**
     * Process result
     */
    private static String status = null;

    /**
     * Active Waiting banner
     */
    public static void printBanner() {
        new Thread(new Runnable() {
            public void run() {
                int x =0;
                String anim= "|/-\\";
                while (active) {
                        String data = "\r" + "Please wait " + anim.charAt(x % anim.length());
                        try {
                            System.out.write(data.getBytes());
                            Thread.sleep(Constants.WAITING_TIME);
                        } catch (Exception e) {}
                    x++;
                }
                try {
                    System.out.write(("\rProcess " + status + "...").getBytes());
                    Thread.sleep(Constants.WAITING_TIME);
                    System.out.println(" ");
                } catch (Exception e) {}
               
            }
        }).start();
    }
    
    /**
     * Stop waiting banner
     * @param success <true> print success in console 
     */
    public static void stopBanner(boolean success) {
        active = false;
        status = (success) ? "Completed" : "Fail";
        try {
            Thread.sleep(Constants.WAITING_TIME);
        } catch (Exception e) {}
        active = true;
    }
}
