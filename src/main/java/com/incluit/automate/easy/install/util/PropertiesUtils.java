package com.incluit.automate.easy.install.util;

import org.apache.commons.configuration.ConfigurationException;
import org.apache.commons.configuration.PropertiesConfiguration;

import com.incluit.automate.easy.install.Core;

/**
 * Property Utils (info.properties)
 * @author pmallo
 */
public class PropertiesUtils {

    /**
     * Add a property in info.properties
     * @param propertyName property name
     * @param value property value
     * @throws ConfigurationException
     */
    public static void addAProperty(String propertyName, String value) throws ConfigurationException {
        
        PropertiesConfiguration config = new PropertiesConfiguration();
        config.load(Core.PROJECT_PATH + Constants.INFO_PROPERTIES_PATH);
        config.setProperty(propertyName, value);
        config.save(Core.PROJECT_PATH + Constants.INFO_PROPERTIES_PATH);
    }
}
