package com.incluit.automate.easy.install.util;

import java.io.File;


public interface Constants {

    static final String AUTOMATE_VERSION = "1.9.0-alpha";

    static final String MAVEN_COMMAND = "mvn archetype:generate -DgroupId=automate"
            + " -DartifactId=%s -DarchetypeVersion=1.3 -DarchetypeGroupId=org.apache.maven.archetypes "
            + "-DarchetypeArtifactId=maven-archetype-quickstart -Dversion=%s -DinteractiveMode=false";
    
    
    static final String DEFAULT_PROJECT_VERSION = "1.0";
    static final String BUILD_SUCCESS_TEXT = "BUILD SUCCESS";
    
    static final String PROJECT_BASE_PATH = System.getProperty("user.dir") + File.separator;
    static final int EMPTY_FIELD = 0;
    
    static final int WAITING_TIME = 100;
    
    static final String JAVA_RESOURCES = "/src/main/resources";
    static final String JAVA_RESOURCES_APK = "/src/main/resources/apk";
    static final String JAVA_JAVA = "/src/main/java";
    static final String BASE_PROJECT_PATH = "";
    static final String JAVA_STORIES = "/src/main/stories";
    static final String TEST_RESOURCES = "/src/test/resources";
    
    static final String TEST_DEFAULT_CLASS = "/src/test/java/automate/AppTest.java";
    static final String TEST_DEFAULT_PACKAGE = "/src/test/java/automate";
    static final String TEST_JAVA_FOLDER = "/src/test/java";
    static final String JAVA_DEFAULT_CLASS = "/src/main/java/automate/App.java";
    static final String JAVA_DEFAULT_PACKAGE = "/src/main/java/automate";
    static final String INFO_PROPERTIES_PATH = "src/main/resources/info.properties";
    
    static final String POM_XML = "pom.xml";
    static final String GROUP_ID = "groupId";
    
    static final String PROPERTIES_FILE_NAME = "info.properties";
    static final String POM_FILE_NAME = "pom.xml";
    static final String SPRING_FILE_NAME = "steps-context.xml";
    static final String HELLO_WORLD_FOLDER = "/hw/";
    static final String HELLO_WORLD_STORY_NAME = "test1.story";
    static final String HELLO_WORLD_SUITE_NAME = "firstSuite.xml";
    static final String HELLO_WORLD_SUITE_JAVA_CLASS = "MySteps.java";
    static final String HELLO_WORLD_PACKAGE = "/src/main/java/steps";
    
    static final String CONSOLE_BANNER_FILE_NAME = "banner.txt";
    static final String PROJECT_NAME_FILE_NAME = "pn.txt";
    
    static final String DEFAULT_GROUP_ID = "com.incluit.automate";
    
    static final String DOWNLOAD_JAR_NAME = "download-jar.sh";
    static final String DOWNLOAD_JAR_CONTENT = "mkdir -p $HOME/.m2/repository/com/incluit/automate/$2/$3/\n" +
            "curl -s https://gitlab.com/AutoMATE-HA/AutoMATEDependecies/raw/master/$1.jar --output  $HOME/.m2/repository/com/incluit/automate/$2/$3/$4.jar";
    static final String DOWNLOAD_JAR_SH_PERMISSION = "rwxrwxrwx";
    
    
    // Sauce Labs keys 
    static final String MODULE_USED = "module.used";
    static final String APP_KEY = "app.key";
    static final String ANDROID_DEVICE_VERSION = "android.version";
    static final String ANDROID_DEVICE_NAME = "android.device.name";
    static final String SAUCE_LABS_AUTHETYCATION_KEY = "sauce.labs.authetication.key";
    static final String SAUCE_LABS_USERNAME = "sauce.labs.username";
    static final String MODULE_USED_SAUCE_LABS = "SauceLabs";
    
    static final String MODULE_SAUCE_LABS_ARTIFACT_ID = "module-sauceLabs";
    static final String MODULE_SAUCE_LABS_VERSION = "1.0.0";
    
    
    
    
    

    static enum Modules {
        SAUCE_LABS_ANDROID_NATIVO("SauceLabs", "Android-Native",  "SuaceLabsAndroidNativeModule", Constants.MODULE_SAUCE_LABS_VERSION , " ");
       
        
        private String platform;
        
        private String appType;
        
        private String moduleName;
        
        private String selected;
        
        private String version;
        
        Modules(String platform, String appType, String moduleName,  String version, String selected) {
            this.platform = platform;
            this.appType = appType;
            this.moduleName = moduleName;
            this.selected = selected;
            this.version = version;
        }
        
        public void select() {
            this.selected = "X";
        }
        
        public String getName() {
            return this.moduleName;
        }
        
        public String status() {
            return this.selected;
        }
        
        public String getPlatform() {
            return this.platform;
        }
        
        public String getAppType() {
            return this.appType;
        }
        
        public String getVersion() {
            return this.version;
        }
        public static boolean allModulesSelected() {
            boolean allSelected = true;
            for (int i = 0; i < Modules.values().length; i++) {
                if(!Modules.values()[i].status().contains("X")) {
                    allSelected = false;
                    break;
                }
            }
            return allSelected;
        }
    }
}
