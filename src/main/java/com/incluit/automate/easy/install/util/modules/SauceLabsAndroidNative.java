package com.incluit.automate.easy.install.util.modules;

import java.io.File;

import org.apache.commons.configuration.ConfigurationException;

import com.incluit.automate.easy.install.util.Constants;
import com.incluit.automate.easy.install.util.FileUtils;
import com.incluit.automate.easy.install.util.PomDependeciesUtils;
import com.incluit.automate.easy.install.util.PropertiesUtils;

/**
 * Add module SauceLabsAndroidNative in project
 * @author pmallo
 */
public class SauceLabsAndroidNative {
   
    /**
     * Add SauceLabsAndroidNative module in new project
     * @throws Exception
     */
    public static void addModule() throws Exception {
        createFolders();
        addKeys();
        FileUtils.downloadJarAndInstall(Constants.MODULE_SAUCE_LABS_ARTIFACT_ID, Constants.MODULE_SAUCE_LABS_VERSION);
        PomDependeciesUtils.addDependency(Constants.MODULE_SAUCE_LABS_ARTIFACT_ID, Constants.MODULE_SAUCE_LABS_VERSION);
    }
    
    /**
     * Create folder for module
     */
    private static void createFolders() {
        new File(Constants.JAVA_RESOURCES_APK).mkdirs();
    }
    
    /**
     * Add key in info.properties
     * @throws ConfigurationException
     */
    private static void addKeys() throws ConfigurationException {

        PropertiesUtils.addAProperty(Constants.MODULE_USED, Constants.MODULE_USED_SAUCE_LABS);
        PropertiesUtils.addAProperty(Constants.APP_KEY, "0");
        PropertiesUtils.addAProperty(Constants.ANDROID_DEVICE_VERSION, " ");
        PropertiesUtils.addAProperty(Constants.ANDROID_DEVICE_NAME, " ");
        PropertiesUtils.addAProperty(Constants.SAUCE_LABS_AUTHETYCATION_KEY, " ");
        PropertiesUtils.addAProperty(Constants.SAUCE_LABS_USERNAME, " ");
        
    }
    
    
    
}
