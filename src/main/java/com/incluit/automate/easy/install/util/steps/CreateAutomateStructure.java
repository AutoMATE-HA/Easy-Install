package com.incluit.automate.easy.install.util.steps;

import java.io.File;
import java.io.InputStream;

import org.apache.commons.io.IOUtils;

import com.incluit.automate.easy.install.Core;
import com.incluit.automate.easy.install.util.Constants;
import com.incluit.automate.easy.install.util.FileUtils;

/**
 * Create stucture for basic AutoMATE project 
 * @author pmallo
 */
public class CreateAutomateStructure {

    /**
     * Create basic steucture 
     * @param createHWProject <true> is a Hello World project
     * @param autoMATEVersion Core POM version
     * @throws Exception
     */
    public static void createStructure(boolean createHWProject, String autoMATEVersion)
            throws Exception {

        new File(Core.PROJECT_PATH + Constants.JAVA_RESOURCES).mkdirs();
        new File(Core.PROJECT_PATH + Constants.JAVA_JAVA).mkdirs();
        new File(Core.PROJECT_PATH + Constants.TEST_RESOURCES).mkdirs();
        new File(Core.PROJECT_PATH + Constants.JAVA_STORIES).mkdirs();
        new File(Core.PROJECT_PATH + Constants.TEST_DEFAULT_CLASS).delete();
        new File(Core.PROJECT_PATH + Constants.TEST_DEFAULT_PACKAGE).delete();
        new File(Core.PROJECT_PATH + Constants.TEST_JAVA_FOLDER).delete();
        new File(Core.PROJECT_PATH + Constants.JAVA_DEFAULT_CLASS).delete();
        new File(Core.PROJECT_PATH + Constants.JAVA_DEFAULT_PACKAGE).delete();
        
        if (!createHWProject) {

            File infoPorperties = new File(Core.PROJECT_PATH + Constants.JAVA_RESOURCES
                    + File.separator + Constants.PROPERTIES_FILE_NAME);
            InputStream inputStream = CreateAutomateStructure.class.getClass()
                    .getResourceAsStream(File.separator + Constants.PROPERTIES_FILE_NAME);
            byte[] data = IOUtils.toByteArray(inputStream);
            org.apache.commons.io.FileUtils.writeByteArrayToFile(
                    infoPorperties, data);

            File stepsContext = new File(Core.PROJECT_PATH + Constants.JAVA_RESOURCES
                    + File.separator + Constants.SPRING_FILE_NAME);
            inputStream = CreateAutomateStructure.class.getClass()
                    .getResourceAsStream(File.separator + Constants.SPRING_FILE_NAME);
            data = IOUtils.toByteArray(inputStream);
            org.apache.commons.io.FileUtils.writeByteArrayToFile(stepsContext,
                    data);

        } else {

            FileUtils.copyFile(Constants.HELLO_WORLD_FOLDER + Constants.PROPERTIES_FILE_NAME, Constants.JAVA_RESOURCES);
            FileUtils.copyFile(Constants.HELLO_WORLD_FOLDER + Constants.SPRING_FILE_NAME, Constants.JAVA_RESOURCES);
            FileUtils.copyFile(Constants.HELLO_WORLD_FOLDER + Constants.HELLO_WORLD_STORY_NAME, Constants.JAVA_STORIES);
            FileUtils.copyFile(Constants.HELLO_WORLD_FOLDER + Constants.HELLO_WORLD_SUITE_NAME, Constants.TEST_RESOURCES);
            new File(Core.PROJECT_PATH + Constants.HELLO_WORLD_PACKAGE).mkdirs();
            FileUtils.copyFile(Constants.HELLO_WORLD_FOLDER + Constants.HELLO_WORLD_SUITE_JAVA_CLASS, Constants.HELLO_WORLD_PACKAGE);
        }

    }
    
}
