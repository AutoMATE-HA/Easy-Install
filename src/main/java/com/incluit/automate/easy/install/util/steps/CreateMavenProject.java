package com.incluit.automate.easy.install.util.steps;

import java.io.BufferedReader;
import java.io.BufferedWriter;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileWriter;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.nio.charset.Charset;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.util.Arrays;
import java.util.List;

import org.beryx.textio.TextIO;

import com.incluit.automate.easy.install.Core;
import com.incluit.automate.easy.install.util.Constants;
import com.incluit.automate.easy.install.util.FileUtils;
import com.incluit.automate.easy.install.util.WaitingBanner;

/**
 * Create maven project class
 * @author pmallo
 */
public class CreateMavenProject {

    /**
     * Tags to remplaze in pom template
     * @author pmallo
     *
     */
    private static enum Tags {
        AUTOMATE_VERSION, PROJECT_NAME, PROJECT_VERSION;
        
        public static boolean existKey(String line) {
            for (Tags tag : Tags.values()) {
                if(line.contains(tag.toString())) {
                    return true;
                }
            }
            return false;
        }
    }
    
    /**
     * New project version
     */
    private static String version;

    /**
     * Create a new AutoMATE project 
     * @param textIO console
     * @return <true> process success
     * @throws Exception
     */
    public static boolean createProyect(TextIO textIO) throws Exception {

        textIO.getTextTerminal()
                .println(
                        "------------------------------------------------------------------");
        textIO.getTextTerminal().println(
                "Create maven project with AutoMATE structure");
        textIO.getTextTerminal()
                .println(
                        "------------------------------------------------------------------");
        textIO.getTextTerminal().println("");
        String artifactId = textIO.newStringInputReader()
                .read("Project name: ");
        Core.PROJECT_NAME = artifactId;
        Core.PROJECT_PATH = Constants.PROJECT_BASE_PATH + Core.PROJECT_NAME + File.separator;
        version = textIO
                .newStringInputReader()
                .withMinLength(Constants.EMPTY_FIELD)
                .read("Version [ENTER for default version "
                        + Constants.DEFAULT_PROJECT_VERSION + "]: ");
        version = (version.isEmpty()) ? Constants.DEFAULT_PROJECT_VERSION
                : version;

        System.out.println(" ");
        System.out.println(" ");
        System.out.println("Creating Maven project.");
        WaitingBanner.printBanner();

        
        List<String> lines = Arrays.asList(Core.PROJECT_NAME);
        Path file = Paths.get(Constants.PROJECT_NAME_FILE_NAME);
        Files.write(file, lines, Charset.forName("UTF-8"));
        
        new File(Core.PROJECT_PATH).mkdir();
        FileUtils.copyFile(File.separator + Constants.POM_FILE_NAME, Constants.BASE_PROJECT_PATH);
        
        completePom();
        WaitingBanner.stopBanner(true);
        textIO.getTextTerminal().println(" ");
        return true;

    }
    
    /**
     * Replace keys in pom template 
     * @throws Exception
     */
    private static void completePom() throws Exception{

        File pom = new File(Core.PROJECT_PATH + Constants.POM_XML);
        
        InputStream inputStream = new FileInputStream(pom);

        BufferedReader rdr = new BufferedReader(new InputStreamReader(
                inputStream));
        String line = null;
        StringBuffer buffer = new StringBuffer();
        try {
            while ((line = rdr.readLine()) != null) {
                if(Tags.existKey(line)) {
                    buffer.append(replaceValue(line,Tags.valueOf( line.split(">")[1].split("<")[0])) + "\n");
                } else {
                    buffer.append(line + "\n");
                }
                
            }
            rdr.close();
        } catch (IOException e) {
            e.printStackTrace();
        }
        
        BufferedWriter out = new BufferedWriter(new FileWriter(pom));
        out.write(buffer.toString());
        out.flush();
        out.close();
    }
    
    /**
     * Replace tags values
     * @param line to read
     * @param tag to replace value
     * @return
     */
    private static String replaceValue(String line, Tags tag){
        switch (tag) {
        case AUTOMATE_VERSION:
            return line.replace(Tags.AUTOMATE_VERSION.toString(),Constants.AUTOMATE_VERSION);
        case PROJECT_NAME:
            return line.replace(Tags.PROJECT_NAME.toString(), Core.PROJECT_NAME);
        case PROJECT_VERSION:
            return line.replace(Tags.PROJECT_VERSION.toString(), version);
        }
        return null;
    }
    
    
}
