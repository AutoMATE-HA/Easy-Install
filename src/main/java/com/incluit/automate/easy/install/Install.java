package com.incluit.automate.easy.install;

import org.beryx.textio.TextIO;
import org.beryx.textio.TextIoFactory;

import com.incluit.automate.easy.install.util.Constants;
import com.incluit.automate.easy.install.util.steps.CreateMavenProject;

/**
 * Main class
 * @author pmallo
 *
 */
public class Install {
    
    /**
     * Main Method
     * @param args
     * @throws Exception
     */
    public static void main(String[] args) throws Exception {
       
        TextIO textIO = TextIoFactory.getTextIO();
        Core core = new Core(textIO);
        
        core.printAutoMATEBanner();
        
        CreateMavenProject.createProyect(textIO);
        
        textIO.getTextTerminal().println(" ");
        
        core.isAHelloWorldProject(textIO.newStringInputReader()
                .withMinLength(Constants.EMPTY_FIELD).read("Add files for a 'Hello World' Project [y/N]: "));
        
         core.addAutoMATEModules();        
        
    }

}
