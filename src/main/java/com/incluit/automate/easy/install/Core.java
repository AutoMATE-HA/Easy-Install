package com.incluit.automate.easy.install;

import java.nio.charset.Charset;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.util.ArrayList;
import java.util.List;

import org.beryx.textio.TextIO;

import com.incluit.automate.easy.install.util.Constants;
import com.incluit.automate.easy.install.util.Constants.Modules;
import com.incluit.automate.easy.install.util.modules.SauceLabsAndroidNative;
import com.incluit.automate.easy.install.util.steps.CreateAutomateStructure;

/**
 * Install Core Process 
 * @author pmallo
 */
public class Core {
    
    /**
     * Console Object
     */
    private TextIO textIO;
    
    /**
     * Constructor
     * @param textIO console object
     */
    public Core (TextIO textIO) {
        this.textIO = textIO;
    }
    
    public static String PROJECT_PATH = "";
    public static String PROJECT_NAME = "";

    /**
     * Print AutoMATE banner
     */
    public void printAutoMATEBanner() {
        
        textIO.getTextTerminal().println("------------------------------------------------------------------------------------------------");
        textIO.getTextTerminal().println("    :::     :::    ::: :::::::::::  ::::::::  ::::    ::::      :::     ::::::::::: :::::::::: ");
        textIO.getTextTerminal().println("  :+: :+:   :+:    :+:     :+:     :+:    :+: +:+:+: :+:+:+   :+: :+:       :+:     :+:        ");
        textIO.getTextTerminal().println(" +:+   +:+  +:+    +:+     +:+     +:+    +:+ +:+ +:+:+ +:+  +:+   +:+      +:+     +:+        ");
        textIO.getTextTerminal().println("+#++:++#++: +#+    +:+     +#+     +#+    +:+ +#+  +:+  +#+ +#++:++#++:     +#+     +#++:++#   ");
        textIO.getTextTerminal().println("+#+     +#+ +#+    +#+     +#+     +#+    +#+ +#+       +#+ +#+     +#+     +#+     +#+        ");
        textIO.getTextTerminal().println("#+#     #+# #+#    #+#     #+#     #+#    #+# #+#       #+# #+#     #+#     #+#     #+#        ");
        textIO.getTextTerminal().println("###     ###  ########      ###      ########  ###       ### ###     ###     ###     ########## ");
        textIO.getTextTerminal().println("                                                                            Powered by Incluit ");
        textIO.getTextTerminal().println("------------------------------------------------------------------------------------------------");
        textIO.getTextTerminal().println(" ");
        textIO.getTextTerminal().println("Welcome to AutoMATE Easy Install");
        textIO.getTextTerminal().println("--------------------------------");
        textIO.getTextTerminal().println(" ");
        textIO.getTextTerminal().println(" ");
    }
    
    /**
     * Add class for Hello World project
     * @param answer <y> add files
     * @throws Exception
     */
    public void isAHelloWorldProject(String answer) throws Exception{

        boolean createHWProject = (answer.isEmpty() || answer.toUpperCase().equals("N")) ? false : true;
        
        CreateAutomateStructure.createStructure(createHWProject, Constants.AUTOMATE_VERSION);

        List<String> lines = new ArrayList<String>();
        Path file = Paths.get(Constants.CONSOLE_BANNER_FILE_NAME);
        
        if (createHWProject) {
            lines.add("\\n---------------------------------------------------------------------------------\\n"
            + "To execute 'First suite' go to '" + PROJECT_NAME + "' directory and run command:\\n"
            + "mvn clean install -Dsuite=firstSuite\\n"
            + "---------------------------------------------------------------------------------\\n");
        } else {
            lines.add(" ");
        }
        Files.write(file, lines, Charset.forName("UTF-8"));

    }
    
    /**
     * Add Automate Modules in AutoMATE project created
     * @throws Exception
     */
    public void addAutoMATEModules() throws Exception {
        
        textIO.getTextTerminal().println(" ");
        textIO.getTextTerminal()
        .println("------------------------------------------------------------------");
        textIO.getTextTerminal().println("Add AutoMATE Modules in your project");
        textIO.getTextTerminal()
        .println("------------------------------------------------------------------");

        textIO.getTextTerminal().println(" ");
        String addModule = textIO.newStringInputReader()
                .withMinLength(Constants.EMPTY_FIELD).read("Add an AutoMATE module [Y/n] ");
        
        if ((addModule.isEmpty() || addModule.toUpperCase().equals("Y")) ? true : false) {
            textIO.getTextTerminal().println("Please select a module from the list by Id. ");
       
            do {
                printModuleList();
            
                int moduleSelected = Integer.valueOf(textIO.newStringInputReader()
                  .withMinLength(Constants.EMPTY_FIELD).read("Please select a module from list by Id: "));
            
                if(Modules.values()[moduleSelected].status().contains("X")) {
                    textIO.getTextTerminal().println(" ");
                    textIO.getTextTerminal().println("The " + Modules.values()[moduleSelected].getName() +
                            " module has already been applied");
                } else {
                        Modules.values()[moduleSelected].select();
                        addModule(Modules.values()[moduleSelected]);
                }
                
                textIO.getTextTerminal().println(" ");
                if(!Modules.allModulesSelected()) {
                    textIO.getTextTerminal().println(" ");
                addModule = textIO.newStringInputReader()
                        .withMinLength(Constants.EMPTY_FIELD).read("Add other AutoMATE module [y/N] ");
                } else {
                    addModule = "N";
                    textIO.getTextTerminal().println("All the modules were applied in your project");
                }
            } while((addModule.isEmpty() || addModule.toUpperCase().equals("N")) ? false : true) ;
        
        }
    }
    
    /**
     * Print modules list soported
     */
    public void printModuleList() {
        textIO.getTextTerminal().println("-------------------------------------------------------------------------------------------------------------------------");
        textIO.getTextTerminal().println("|                                    AutoMATE Modules list                                                              |");
        textIO.getTextTerminal().println("|-----------------------------------------------------------------------------------------------------------------------|");
        textIO.getTextTerminal().println("| Id | Applied | Platform          | App type           | Module Name                                 | Version         |");
        textIO.getTextTerminal().println("|----|---------|-------------------|--------------------|---------------------------------------------|-----------------|");
        for (int i = 0; i < Modules.values().length; i++) {
            textIO.getTextTerminal().println(
                    "| " + String.format("%1$" + 2 + "s", i) + " " +
                    "|    " + Modules.values()[i].status() + "    | " +
                    String.format("%1$-" + 17 + "s", Modules.values()[i].getPlatform()) + " | " +
                    String.format("%1$-" + 18 + "s", Modules.values()[i].getAppType()) + " | " +
                    String.format("%1$-" + 43 + "s", Modules.values()[i].getName()) + " | " +
                    String.format("%1$-" + 15 + "s", Modules.values()[i].getVersion()) + " | "
                       
        +"");
        }
        textIO.getTextTerminal().println("-------------------------------------------------------------------------------------------------------------------------");
          
    }
    
    /**
     * Apply an module
     * @param moduleSelected module to apply
     * @throws Exception
     */
    public void addModule(Modules moduleSelected) throws Exception {
        switch (moduleSelected) {
        case SAUCE_LABS_ANDROID_NATIVO:
            SauceLabsAndroidNative.addModule();
            break;

        default:
            break;
        }
    }
}
